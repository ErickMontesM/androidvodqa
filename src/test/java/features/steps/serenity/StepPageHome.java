package features.steps.serenity;

import java.net.MalformedURLException;

import net.thucydides.core.annotations.Step;
import pages.PageHome;

public class StepPageHome {
	PageHome pageH;
	
	@Step
	public void ingresarUsuario(String user,String pass) throws MalformedURLException, InterruptedException {
		
		pageH.ingresarUsuario(user, pass);
	}
	
	@Step
	public void bajamosSlide() {
		pageH.mover();
	}
	
	
	
}
