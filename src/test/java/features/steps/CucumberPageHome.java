package features.steps;

import java.net.MalformedURLException;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import features.steps.serenity.StepPageHome;
import net.thucydides.core.annotations.Steps;

public class CucumberPageHome {

	@Steps
	StepPageHome stepHome;
	
	@Given("^ingresar (.*) y (.*)$")
	public void ingresarUsuario(String user,String pass) throws MalformedURLException, InterruptedException {
	
		stepHome.ingresarUsuario(user, pass);
	}
	
	@When("^bajamos el slide$")
	public void bajamosSlide() {
		stepHome.bajamosSlide();
	}
	
	
}
