package features;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = { "json:target/sire/serenity/cucumber.json" }, features = {
		"src//test//resources//features//vodkaqa.feature" })
public class RunnerTest {

}
