package pages;


import java.awt.Point;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.webdriver.WebDriverFacade;



public class PageHome extends PageObject {
//	WebDriver dr;
	@FindBy(name="username")
	private WebElement usuario;
	
	@FindBy(xpath="//android.widget.EditText[@content-desc='password']")
	private WebElement contraseña;
	
	@FindBy(xpath="//android.widget.TextView[@text='LOG IN']")
	private WebElement botonLogin;
	
	@FindBy(xpath="//android.widget.TextView[@content-desc='carousel']")
	private WebElement carousel;
	
	public void ingresarUsuario(String user,String pass) {
		WebDriverWait wait=new WebDriverWait(getAllWebDriver(), 20);
		wait.until(ExpectedConditions.visibilityOf(botonLogin));
		
		botonLogin.click();
	}
	
	public void mover() {
		
			TouchAction act = new TouchAction((MobileDriver) getAllWebDriver());
			int anchor = (int) (1920 * 0.55);
			int startPoint = (int) (1080* 0.9);
			int endPoint = (int) (1080 * 0.01);
			act.press(PointOption.point(startPoint, anchor)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2))).moveTo(PointOption.point(endPoint, anchor)).release().perform();
			WebDriverWait wait=new WebDriverWait(getAllWebDriver(), 20);
			wait.until(ExpectedConditions.visibilityOf(carousel));
			carousel.click();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	}
	private WebDriver getAllWebDriver() {
		WebDriverFacade facade = (WebDriverFacade) getDriver();
		return facade.getProxiedDriver();
	}

	

}
